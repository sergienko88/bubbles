﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    #region Variables

    [SerializeField]
    GameObject ScoreUIObject;

    [SerializeField]
    Text EndScoreText;

    [SerializeField]
    Text ScoreText;

    [SerializeField]
    Text TimerText;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.OnFinishGame += OnFinishGame;
        GameManager.Instance.OnUpdateScore += OnUpdateScore;
        GameManager.Instance.OnStartGame += OnUpdateScore;
        GameManager.Instance.OnUpdateTimer += OnUpdateTimer;
    }

    #region business functions

    void OnFinishGame()
    {
        if (ScoreUIObject != null)
        {
            ScoreUIObject.SetActive(true);
        }
        if (EndScoreText != null)
        {
            EndScoreText.text = "Score : " + GameManager.Instance.Score.ToString();
        }
    }

    void OnUpdateScore()
    {
        if (ScoreText != null)
        {
            ScoreText.text = "Score : " + GameManager.Instance.Score.ToString();
        }
    }

    void OnUpdateTimer()
    {
        if (TimerText != null)
        {
            TimerText.text = "Timer : " + (int)GameManager.Instance.CurrentTime + "/" + GameManager.Instance.RoundTime;
        }
    }

    #endregion

    private void OnDestroy()
    {
        if (GameManager.Instance)
        {
            GameManager.Instance.OnFinishGame -= OnFinishGame;
            GameManager.Instance.OnUpdateScore -= OnUpdateScore;
            GameManager.Instance.OnStartGame -= OnUpdateScore;
            GameManager.Instance.OnUpdateTimer -= OnUpdateTimer;
        }
    }
}
