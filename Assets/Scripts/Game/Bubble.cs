﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D),typeof(SpriteRenderer))]
public class Bubble : MonoBehaviour
{
    #region Variables

    Transform cachedTransform;

    CircleCollider2D circleCollider2D;

    SpriteRenderer spriteRenderer;

    int points = 10;

    float baseSpeed = 1f;

    float defaultRadius = .5f;

    Vector2 defaultDrawMode = new Vector2(1f, 1f);

    #endregion

    #region Properties

    public CircleCollider2D BubbleCircleCollider2D
    {
        get { return circleCollider2D; }
    }

    public Transform CTransform
    {
        get
        {
            return cachedTransform == null ? transform : cachedTransform;
        }
    }

    public int Points
    {
        get
        {
            return points;
        }
    }

    public float Speed { get { return baseSpeed + GameManager.Instance.IncreasedSpeed; } }

    public SpriteRenderer BubbleSpriteRenderer
    {
        get
        {
            return spriteRenderer;
        }
    }

    public Action <Bubble> OnHide
    {
        get
        {
            return onHide;
        }

        set
        {
            onHide = value;
        }
    }

    public Action<Bubble> OnClick
    {
        get
        {
            return onClick;
        }

        set
        {
            onClick = value;
        }
    }

    #endregion

    #region Actions

    Action <Bubble> onHide;

    Action<Bubble> onClick;
    #endregion

    #region Initializes

    private void Awake()
    {
        cachedTransform = transform;
        circleCollider2D = GetComponent<CircleCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        defaultRadius = circleCollider2D.radius;
        defaultDrawMode = spriteRenderer.size;
    }

    public void Initialize(BubbleProperties properties)
    {
        points = properties.Points;
        spriteRenderer.color = properties.Color;
        baseSpeed = properties.BaseSpeed;
        circleCollider2D.radius = properties.Radius;
        float size = properties.Radius * defaultDrawMode.x / defaultRadius;
        spriteRenderer.size = new Vector2(size, size);        
    }
    
    #endregion
    
    // Update is called once per frame
    void Update()
    {
        CTransform.position += Vector3.up * Time.deltaTime * Speed;
        if (CTransform.position.y > (Camera.main.ScreenToWorldPoint(new Vector3(0,Screen.height,0)) + new Vector3(0, spriteRenderer.size.y/2,0) ).y )
        {
            Hide();
        }
    }

    private void OnMouseDown()
    {   
        onClick?.Invoke(this);
        onClick = null;
        Hide();
    }

    void Hide()
    {
        onHide?.Invoke(this);
        onHide = null;
        gameObject.SetActive(false);
    }
}
