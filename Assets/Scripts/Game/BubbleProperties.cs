﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleProperties
{
    #region Variables

    float radius = .5f;
    Color color = Color.green;
    float baseSpeed = 1f;
    int points = 1;

    #endregion

    #region ctors

    public BubbleProperties()
    {

    }

    public BubbleProperties(float radius, Color color, float baseSpeed,int points)
    {
        this.radius = radius;
        this.color = color;
        this.baseSpeed = baseSpeed;
        this.points = points;
    }

    #endregion

    #region Properties

    public float Radius
    {
        get
        {
            return radius;
        }
    }

    public Color Color
    {
        get
        {
            return color;
        }
    }

    public float BaseSpeed
    {
        get
        {
            return baseSpeed;
        }
    }

    public int Points
    {
        get
        {
            return points;
        }
    }

    #endregion
}
