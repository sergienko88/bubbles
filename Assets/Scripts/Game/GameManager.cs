﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    #region Variables

    float roundTime = 20f;

    float currentTime = 0;

    float minIncreasedSpeed = 0f;

    float maxIncreasedSpeed = 3f;

    GameState gameState = GameState.NotInGame;
    int score = 0;

    #endregion

    #region Properties

    public float IncreasedSpeed { get { return Mathf.Lerp(minIncreasedSpeed, maxIncreasedSpeed, CurrentTime / roundTime); } }

    public float CurrentTime
    {
        get
        {
            return currentTime;
        }
    }

    public GameState GameState
    {
        get
        {
            return gameState;
        }
    }

    public Action OnFinishGame
    {
        get
        {
            return onFinishGame;
        }
        set
        {
            onFinishGame = value;
        }
    }

    public int Score
    {
        get
        {
            return score;
        }
    }

    public Action OnUpdateScore
    {
        get
        {
            return onUpdateScore;
        }

        set
        {
            onUpdateScore = value;
        }
    }

    public Action OnStartGame
    {
        get
        {
            return onStartGame;
        }

        set
        {
            onStartGame = value;
        }
    }

    public Action OnUpdateTimer
    {
        get
        {
            return onUpdateTimer;
        }

        set
        {
            onUpdateTimer = value;
        }
    }

    public float RoundTime
    {
        get
        {
            return roundTime;
        }
    }

    #endregion

    #region Actions

    Action onStartGame;

    Action onFinishGame;

    Action onUpdateScore;

    Action onUpdateTimer;

    #endregion

    public void StartGame()
    {
        score = 0;
        currentTime = 0;
        gameState = GameState.Game;
        onStartGame?.Invoke();
    }

    public void StopGame()
    {
        gameState = GameState.NotInGame;
        onFinishGame?.Invoke();
    }

    public void AddScore(int value)
    {
        score += value;
        onUpdateScore?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        switch (gameState)
        {
            case GameState.Game:
                if (currentTime < roundTime)
                {
                    currentTime += Time.deltaTime;
                    onUpdateTimer?.Invoke();
                }
                else
                {
                    StopGame();
                }
                break;
            case GameState.NotInGame:
                break;
            default:
                break;
        }        
    }
}
