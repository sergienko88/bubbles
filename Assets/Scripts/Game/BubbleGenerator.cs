﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleGenerator : MonoBehaviour
{
    #region Variables

    float timer = 0;

    float duration = .1f;

    List<BubbleProperties> bubbleProps = new List<BubbleProperties> {
            new BubbleProperties(.5f,Color.green,3f,10),
            new BubbleProperties(.7f,Color.cyan,2.5f,7),
            new BubbleProperties(1f,Color.blue,2f,5),
            new BubbleProperties(1.5f,Color.gray,1.7f,3),
            new BubbleProperties(1.7f,Color.magenta,1.5f,2),
            new BubbleProperties(1.9f,Color.yellow,1,1)
    };
    ObjectPool<Bubble> bubblePool = new ObjectPool<Bubble>();

    #endregion

    #region Initialize
    // Start is called before the first frame update
    void Start()
    {
        duration = UnityEngine.Random.value;
        bubblePool.Initialize("Bubble");

        GameManager.Instance.OnFinishGame += OnFinishGame;
    }

    void InitializeBubble(Bubble bubble)
    {
        bubble.Initialize(bubbleProps[Random.Range(0, bubbleProps.Count - 1)]);
        bubble.OnHide = OnBubbleHide;
        bubble.OnClick = OnBubbleClick;
    }

    #endregion

    #region Helper functions

    Bubble GetBubbleFromPool()
    {
        return bubblePool.GetFreeObject();        
    }

    Vector3 GetCorrectPosition(Bubble bubble)
    {
        Vector2 minPos = Camera.main.ScreenToWorldPoint(new Vector3()) + new Vector3(bubble.BubbleSpriteRenderer.size.x/2, bubble.BubbleSpriteRenderer.size.y / 2,0);
        Vector2 maxPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width ,0, 0)) - new Vector3(bubble.BubbleSpriteRenderer.size.x / 2, -bubble.BubbleSpriteRenderer.size.y / 2, 0); ;

        return new Vector3(Random.Range(minPos.x,maxPos.x) ,minPos.y,0);
    }

    #endregion

    void OnBubbleHide(Bubble bubble)
    {

    }

    void OnBubbleClick(Bubble bubble)
    {
        GameManager.Instance.AddScore(bubble.Points);
    }

    void OnFinishGame()
    {
        bubblePool.HideAllObjects();
    }

    // Update is called once per frame
    void Update()
    {

        switch (GameManager.Instance.GameState)
        {
            case GameState.Game:

                timer += Time.deltaTime;
                if (timer > duration)
                {
                    timer = 0;
                    duration = UnityEngine.Random.value;
                    Bubble bubble = GetBubbleFromPool();
                    if (bubble != null)
                    {
                        bubble.gameObject.SetActive(true);
                        InitializeBubble(bubble);
                        bubble.CTransform.position = GetCorrectPosition(bubble);
                    }
                }

                break;
            case GameState.NotInGame:
                break;
            default:
                break;
        }       
    }

    private void OnDestroy()
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.OnFinishGame += OnFinishGame;
        }
    }
}
