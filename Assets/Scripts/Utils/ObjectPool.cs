﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObjectPool <T> where T:MonoBehaviour
{
    #region Variables

    List<T> pool = new List<T>();
    T prefab = null;

    #endregion

    #region Initialize

    public void Initialize(string name)
    {
        prefab = Resources.Load<T>(name);
        if (prefab != null)
        {
            for(int i = 0; i < 10; i++)
            {
                try
                {
                    T obj = UnityEngine.GameObject.Instantiate(prefab);
                    obj.gameObject.SetActive(false);
                    pool.Add(obj);
                }
                catch(Exception e)
                {
                    Debug.Log("Cant Instantiate prefab ["+name +"] with exception : "+e.Message);
                }
            }
        }
        else
        {
            Debug.Log(this + ":cant find prefab [" +name+"]");
        }
    }

    #endregion

    public T GetFreeObject()
    {
        T obj = pool.Find(o=>!o.gameObject.activeSelf);
        if (obj == null && prefab !=null)
        {
            try
            {
                obj = UnityEngine.GameObject.Instantiate(prefab);
                pool.Add(obj);
            }
            catch(Exception e)
            {
                Debug.Log("Cant create object from prefab in pool ["+this+"]");
            }
        }        
        return obj;
    }

    public void HideAllObjects()
    {
        pool.ForEach(o => o.gameObject.SetActive(false));
    }
}
